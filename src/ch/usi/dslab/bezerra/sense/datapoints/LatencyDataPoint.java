/*

 Sense - A library for easy monitoring in Java
 Copyright (C) 2014, University of Lugano
 
 This file is part of Sense.
 
 Sense is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.bezerra.sense.datapoints;

import java.util.List;

public class LatencyDataPoint implements DataPoint {
   private static final long serialVersionUID = 1L;

   long intervalStart, intervalEnd, weight;
   double intervalLatencyAverage;

   public LatencyDataPoint(){}

   public LatencyDataPoint(long intervalStart, long intervalEnd, long weight, double intervalLatencyAverage) {
      this.intervalStart = intervalStart;
      this.intervalEnd = intervalEnd;
      this.weight = weight;
      this.intervalLatencyAverage = intervalLatencyAverage;
   }

   @Override
   public long getInstant() {
      return intervalStart;
   }

   @Override
   public long getEnd() {
      return intervalEnd;
   }

   @Override
   public DataPoint getAverage(List<DataPoint> values) {
      LatencyDataPoint avg = new LatencyDataPoint();
      double totalWeight = 0.0d;
      for (DataPoint dp : values) {
         LatencyDataPoint ldp = (LatencyDataPoint) dp;
         avg.intervalLatencyAverage += ldp.intervalLatencyAverage * ldp.weight;
         totalWeight += ldp.weight;
      }
      avg.intervalLatencyAverage /= totalWeight;
      return avg;
   }

   @Override
   public String getName() {
      return "latency";
   }

   @Override
   public String getLogHeader() {
      return "latency (ns) ";
   }

   @Override
   public String toString() {
      return String.format("%d %d %f", getInstant(), weight, intervalLatencyAverage);
   }

   @Override
   public DataPoint getAggregate(List<DataPoint> values) {
      return null;
   }

   @Override
   public boolean isAggregatable() {
      return false;
   }

}

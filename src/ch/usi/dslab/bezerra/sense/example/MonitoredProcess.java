/*

 Sense - A library for easy monitoring in Java
 Copyright (C) 2014, University of Lugano
 
 This file is part of Sense.
 
 Sense is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.bezerra.sense.example;

import java.util.Random;

import ch.usi.dslab.bezerra.sense.DataGatherer;
import ch.usi.dslab.bezerra.sense.monitors.LatencyPassiveMonitor;
import ch.usi.dslab.bezerra.sense.monitors.ThroughputPassiveMonitor;

public class MonitoredProcess {
   ThroughputPassiveMonitor tpMonitor;
   LatencyPassiveMonitor latencyMonitor;
   Random rand = new Random();
   
   public MonitoredProcess(int pid) {
      tpMonitor      = new ThroughputPassiveMonitor(pid, "myprocess");
      latencyMonitor = new LatencyPassiveMonitor   (pid, "myprocess");
   }
   
   void simulateCommunication() {
      try {Thread.sleep((long) rand.nextInt(100));}
      catch (InterruptedException e) {e.printStackTrace();}
   }

   public static void main(String... args) {
      int duration = 10; // seconds
      
      // this configuration must be done before any other call to any
      // method of Sense is made
      DataGatherer.configure(duration, null, "localhost", 60000);
      
      MonitoredProcess proc = new MonitoredProcess(12345);

      long now = System.currentTimeMillis();
      long end = now + duration * 1000;
      while (now < end) {
         
         long sendTime = System.nanoTime();
         // ==============================
         // simulating that the monitored process send a message and received a reply
         proc.simulateCommunication();
         // ==============================
         long recvTime = System.nanoTime();
         
         proc.tpMonitor.incrementCount();
         proc.latencyMonitor.logLatency(sendTime, recvTime);
         
         now = System.currentTimeMillis();
      }

   }
}

/*

 Sense - A library for easy monitoring in Java
 Copyright (C) 2015, University of Lugano
 
 This file is part of Sense.
 
 Sense is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.bezerra.sense.monitors;

import ch.usi.dslab.bezerra.sense.DataGatherer;
import ch.usi.dslab.bezerra.sense.datapoints.LatencyDistributionDataPoint;
import ch.usi.dslab.bezerra.sense.datapoints.LatencyDistributionDataPoint.BucketSet;

public class LatencyDistributionPassiveMonitor extends PassiveMonitor {

   public static final long DEFAULT_BUCKET_WIDTH_NANO = 10000L; // nanoseconds
   private static long bucketWidthNano = DEFAULT_BUCKET_WIDTH_NANO;
   
   public static long getBucketWidthNano() {
      return bucketWidthNano;
   }

   public static void setBucketWidthNano(long bucketWidthNano) {
      LatencyDistributionPassiveMonitor.bucketWidthNano = bucketWidthNano;
   }
   
   long firstLatencyInstant, lastLatencyInstant;
   BucketSet currentBucketSet;
   
   public LatencyDistributionPassiveMonitor(int monitoredPid, String monitoredName) {
      super(monitoredPid, monitoredName);
      firstLatencyInstant = lastLatencyInstant = 0;
      currentBucketSet = new BucketSet();
   }
   
   public LatencyDistributionPassiveMonitor(int monitoredPid, String monitoredName, boolean recquired) {
      super(monitoredPid, monitoredName, recquired);
      firstLatencyInstant = lastLatencyInstant = 0;
      currentBucketSet = new BucketSet();
   }

   // if receive time is not given, calculate it INSIDE the if
   
   public void logLatencyForDistribution(long sendTimeNano) {
      logLatencyForDistribution(sendTimeNano, 0, true);
   }
   
   public void logLatencyForDistribution(long sendTimeNano, long recvTimeNano) {
      logLatencyForDistribution(sendTimeNano, recvTimeNano, false);
   }
   
   synchronized private void logLatencyForDistribution(long sendTimeNano, long recvTimeNano, boolean calculateRecvTime) {
      if (active == false) {
         return;
      }
      
      // interval average method
      long currentTimeMS = System.currentTimeMillis();
      if (calculateRecvTime) recvTimeNano = System.nanoTime();
      long latency = recvTimeNano - sendTimeNano;
      if (currentBucketSet.isEmpty())
         firstLatencyInstant = currentTimeMS;
      lastLatencyInstant = currentTimeMS;
      
      long latency_bucket = getBucketWidthNano() * (latency/getBucketWidthNano());
      currentBucketSet.increment(latency_bucket);
      
      if (currentTimeMS > lastLogTime + DataGatherer.LOG_INTERVAL_MS) {
         log.add(new LatencyDistributionDataPoint(firstLatencyInstant, lastLatencyInstant, currentBucketSet));
         currentBucketSet = new BucketSet();
         lastLogTime = currentTimeMS;
      }
   }

   @Override
   public void saveToFile() {
      saveToFile(new LatencyDistributionDataPoint());
   }

   @Override
   public void sendToGatherer() {
      sendToGatherer(new LatencyDistributionDataPoint());
   }
}

/*

 Sense - A library for easy monitoring in Java
 Copyright (C) 2014, University of Lugano
 
 This file is part of Sense.
 
 Sense is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 
*/

/**
 * @author Eduardo Bezerra - eduardo.bezerra@usi.ch
 */

package ch.usi.dslab.bezerra.sense.monitors;

import java.util.concurrent.atomic.AtomicLong;

import ch.usi.dslab.bezerra.sense.DataGatherer;
import ch.usi.dslab.bezerra.sense.datapoints.MistakeRateDataPoint;

public class MistakeRatePassiveMonitor extends PassiveMonitor {

   public MistakeRatePassiveMonitor(int monitoredPid, String monitoredName) {
      super(monitoredPid, monitoredName);
   }
   
   public MistakeRatePassiveMonitor(int monitoredPid, String monitoredName, boolean recquired) {
      super(monitoredPid, monitoredName, recquired);
   }

   AtomicLong  currentDeliveryCount = new AtomicLong();
   AtomicLong previousDeliveryCount = new AtomicLong();
   AtomicLong   currentMistakeCount = new AtomicLong();
   AtomicLong  previousMistakeCount = new AtomicLong();

   public void incrementCounts(long deliveries, long mistakes) {
      if (active == false)
         return;
      currentDeliveryCount.addAndGet(deliveries);
      currentMistakeCount .addAndGet(mistakes);
      log();
   }

   synchronized void log() {
      if (active == false) {
         return;
      }
      long now = System.currentTimeMillis();
      if (now > lastLogTime + DataGatherer.LOG_INTERVAL_MS) {
         double deltaDeliveryCount = currentDeliveryCount.get() - previousDeliveryCount.get();
         double deltaMistakeCount  = currentMistakeCount.get()  - previousMistakeCount.get();
         double periodRateOfMistakes = deltaMistakeCount / deltaDeliveryCount;
         log.add(new MistakeRateDataPoint(lastLogTime, now, periodRateOfMistakes));
         lastLogTime = now;
         previousDeliveryCount.set(currentDeliveryCount.get());
         previousMistakeCount .set(currentMistakeCount .get());
      }
   }

   @Override
   public void saveToFile() {
      saveToFile(new MistakeRateDataPoint());  
   }

   @Override
   public void sendToGatherer() {
      sendToGatherer(new MistakeRateDataPoint());
   }
}
